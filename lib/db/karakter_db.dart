import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/models/karakterModel.dart';

class KarakterDatabase {
  static final KarakterDatabase instanceDB = KarakterDatabase._init();

  static Database _database;

  KarakterDatabase._init();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await _initDB('karakter.db');
      return _database;
    }
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idTipe = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final idBool = 'BOOLEAN NOT NULL';

    await db.execute('''CREATE TABLE $namaTabel (
        ${KarakterFields.id} $idTipe,
        ${KarakterFields.namaKarakter} TEXT,
        ${KarakterFields.heightKarakter} TEXT,
        ${KarakterFields.beratKarakter} TEXT,
        ${KarakterFields.tanggalLahir} TEXT,
        ${KarakterFields.kelamin} TEXT,
        ${KarakterFields.films} TEXT,
        ${KarakterFields.species} TEXT,
        ${KarakterFields.isFavorit} $idBool
    )''');
  }

  Future<KarakterModel> createData(KarakterModel karakterModel) async {
    final db = await instanceDB.database;

    final id = await db.insert(namaTabel, karakterModel.toJson());
    return karakterModel.copy(id: id);
  }

  Future<KarakterModel> readKarakter(int id) async {
    final db = await instanceDB.database;

    final maps = await db.query(
      namaTabel,
      columns: KarakterFields.values,
      where: '${KarakterFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return KarakterModel.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found !');
    }
  }

  Future<List<KarakterModel>> readAllFav() async {
    final db = await instanceDB.database;

    final maps = await db.query(
      namaTabel,
      columns: KarakterFields.values,
      where: '${KarakterFields.isFavorit} = ?',
      whereArgs: [1],
    );

    if (maps.isNotEmpty) {
      return maps.map((json) => KarakterModel.fromJson(json)).toList();
    } else {
      throw Exception('No Favorite Karakter !');
    }
  }

  Future<List<KarakterModel>> searchListKarakter(String nama) async {
    final db = await instanceDB.database;

    // final maps = await db.query(
    //   namaTabel,
    //   columns: KarakterFields.values,

    //   where: '${KarakterFields.namaKarakter} = ?',
    //   whereArgs: [nama],
    // );

    final result = await db.rawQuery(
        "SELECT * FROM $namaTabel WHERE ${KarakterFields.namaKarakter}  LIKE '%$nama%' ");

    if (result.isNotEmpty) {
      return result.map((json) => KarakterModel.fromJson(json)).toList();
    } else {
      throw Exception('Data not found !');
    }
  }

  Future<List<KarakterModel>> readAllKarakter() async {
    final db = await instanceDB.database;

    final orderBy = '${KarakterFields.id} ASC';
    final result = await db.query(namaTabel, orderBy: orderBy);
    return result.map((json) => KarakterModel.fromJson(json)).toList();
  }

  Future<int> countSizeTable() async {
    final db = await instanceDB.database;

    final orderBy = '${KarakterFields.id} ASC';
    final result = await db.query(namaTabel, orderBy: orderBy);
    return result.length;
  }

  Future<int> update(KarakterModel karakterModel) async {
    final db = await instanceDB.database;

    return db.update(
      namaTabel,
      karakterModel.toJson(),
      where: '${KarakterFields.id} = ?',
      whereArgs: [karakterModel.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instanceDB.database;

    return await db.delete(
      namaTabel,
      where: '${KarakterFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future deleteDB() async {
    final db = await instanceDB.database;

    db.delete(namaTabel);
  }

  Future close() async {
    final db = await instanceDB.database;

    db.close();
  }
}
