import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/services/sharedpreference.dart';
import 'package:test_majoo/ui/pages/home.dart';
import 'package:test_majoo/ui/pages/splashscreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await UserPreference.init();

  print(UserPreference.getUserLogin());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: FutureBuilder<bool>(
          future: UserPreference.getUserLogin(),
          builder: (buildContext, snapshot) {
            if (snapshot.hasData) {
              return HomePage();
            } else {
              return SplashscreenPage();
            }
          },
        ));
  }
}
