import 'package:flutter/material.dart';
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/models/karakterModel.dart';

class CardKarakter extends StatefulWidget {
  final KarakterModel karakterModel;
  final double width;
  final double height;

  const CardKarakter(this.karakterModel, this.width, this.height);

  @override
  _CardKarakterState createState() => _CardKarakterState();
}

class _CardKarakterState extends State<CardKarakter> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24), color: Colors.amber[100]),
      child: Stack(children: [
        Align(
          alignment: Alignment.topRight,
          child: Container(
            margin: EdgeInsets.only(top: 12, right: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    showAlertDialog(
                        context, "Favorit", 1, widget.karakterModel);
                  },
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(2),
                      child: Icon(
                        Icons.favorite,
                        color: (widget.karakterModel.isFavorit
                            ? Colors.red
                            : Colors.white),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    showAlertDialog(context, "Delete", 2, widget.karakterModel);
                  },
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(2),
                      child: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget.karakterModel.namaKarakter,
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }

  void showAlertDialog(BuildContext buildContext, String title, int tipeDialog,
          KarakterModel karakter) =>
      showDialog(
        context: buildContext,
        builder: (_) => AlertDialog(
          title: Text(title),
          content: Text("Are you sure about that ?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(buildContext).pop();
                },
                child: Text("NO")),
            TextButton(
                onPressed: () async {
                  if (tipeDialog == 1) {
                    final karakterModel =
                        karakter.copy(isFavorit: !karakter.isFavorit);
                    await KarakterDatabase.instanceDB.update(karakterModel);
                    print("favData");
                  } else if (tipeDialog == 2) {
                    await KarakterDatabase.instanceDB.delete(karakter.id);
                    print("deleteData");
                  }
                  setState(() {
                    Navigator.of(buildContext).pop();
                  });
                },
                child: Text("YES")),
          ],
          elevation: 24,
        ),
      );
}

//NOTE : TIPE DIALOG
//1 : FAVORIT
//2 : DELETE
