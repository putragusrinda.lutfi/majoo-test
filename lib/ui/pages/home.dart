import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/services/http_server.dart';
import 'package:test_majoo/ui/pages/beranda.dart';
import 'package:test_majoo/ui/pages/favorites.dart';
import 'package:test_majoo/ui/pages/profile.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int bottomNavBarIndex;
  PageController pageController;

  @override
  void initState() {
    super.initState();

    bottomNavBarIndex = 0;
    pageController = PageController(initialPage: bottomNavBarIndex);

    checkData();
  }

  Future<bool> checkData() async {
    int sizeTabel = await KarakterDatabase.instanceDB.countSizeTable();

    if (sizeTabel == 0) {
      FetchingData.getData();
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: FutureBuilder(
            future: checkData(),
            builder: (_, snapshot) {
              if (snapshot.hasData) {
                bool check = snapshot.data;
                if (check) {
                  return Stack(
                    children: [
                      PageView(
                        controller: pageController,
                        onPageChanged: (index) {
                          setState(() {
                            bottomNavBarIndex = index;
                          });
                        },
                        children: [
                          BerandaPage(),
                          FavoritePage(),
                          ProfilePage()
                        ],
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: 70,
                          child: BottomNavigationBar(
                              onTap: (index) {
                                setState(() {
                                  bottomNavBarIndex = index;
                                  pageController.jumpToPage(index);
                                });
                              },
                              elevation: 0,
                              backgroundColor: Colors.orange,
                              selectedItemColor: Colors.white,
                              unselectedItemColor: Colors.grey,
                              currentIndex: bottomNavBarIndex,
                              selectedLabelStyle:
                                  TextStyle(fontSize: 14, color: Colors.white),
                              items: [
                                BottomNavigationBarItem(
                                    icon: Container(
                                      margin: EdgeInsets.only(bottom: 6),
                                      height: 20,
                                      child: Icon(Icons.home),
                                    ),
                                    label: "Beranda"),
                                BottomNavigationBarItem(
                                    icon: Container(
                                      margin: EdgeInsets.only(bottom: 6),
                                      height: 20,
                                      child: Icon(Icons.favorite),
                                    ),
                                    label: "Favorites"),
                                BottomNavigationBarItem(
                                    icon: Container(
                                      margin: EdgeInsets.only(bottom: 6),
                                      height: 20,
                                      child: Icon(Icons.person),
                                    ),
                                    label: "Favorites"),
                              ]),
                        ),
                      ),
                    ],
                  );
                } else {
                  return SpinKitFadingCircle(
                    size: 50,
                    color: Colors.orange,
                  );
                }
              } else {
                return SpinKitFadingCircle(
                  size: 50,
                  color: Colors.orange,
                );
              }
            }),
      ),
    );
  }
}
