import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/models/karakterModel.dart';
import 'package:test_majoo/ui/pages/detail_karakter.dart';
import 'package:test_majoo/ui/widgets/cardKarakter.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    readFavList();
  }

  Future<List<KarakterModel>> readFavList() async {
    setState(() => isLoading = true);

    List<KarakterModel> daftar = await KarakterDatabase.instanceDB.readAllFav();

    setState(() => isLoading = false);

    return daftar;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            child: ListView(
              children: [
                Container(
                  height: 20,
                ),
                FutureBuilder(
                    future: readFavList(),
                    builder: (_, snapshot) {
                      if (snapshot.hasData) {
                        List<KarakterModel> daftarKarakter = snapshot.data;
                        return Column(
                          children: daftarKarakter
                              .map((e) => Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailKarakterPage(
                                                  idKarakter: e.id,
                                                )),
                                      );
                                    },
                                    child: CardKarakter(
                                      e,
                                      double.infinity,
                                      MediaQuery.of(context).size.height / 5,
                                    ),
                                  )))
                              .toList(),
                        );
                      } else {
                        if (snapshot.hasError) {
                          return Container(
                              height: MediaQuery.of(context).size.height,
                              child:
                                  Center(child: Text("Data Favorit Kosong")));
                        } else {
                          return SpinKitFadingCircle(
                            size: 50,
                            color: Colors.orange,
                          );
                        }
                      }
                    }),
                Container(
                  height: 80,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
