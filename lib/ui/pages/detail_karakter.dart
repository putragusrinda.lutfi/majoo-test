import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/models/karakterModel.dart';
import 'package:test_majoo/ui/pages/home.dart';

class DetailKarakterPage extends StatefulWidget {
  final int idKarakter;

  const DetailKarakterPage({this.idKarakter});

  @override
  _DetailKarakterPageState createState() => _DetailKarakterPageState();
}

class _DetailKarakterPageState extends State<DetailKarakterPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController tinggiController = TextEditingController();
  TextEditingController beratController = TextEditingController();
  TextEditingController lahirController = TextEditingController();

  bool isDataEdited = false;
  bool isDataLengkap = false;

  int _radioValue1 = -1;
  String jenisKelamin = "";
  int correctScore = 0;

  @override
  void initState() {
    super.initState();

    if (widget.idKarakter != null) {
      print("reload init");
      getDataUser();
    }
  }

  Future<KarakterModel> getDataUser() async {
    KarakterModel modelKarakter =
        await KarakterDatabase.instanceDB.readKarakter(widget.idKarakter);

    nameController = TextEditingController(text: modelKarakter.namaKarakter);
    tinggiController =
        TextEditingController(text: modelKarakter.heightKarakter);
    beratController = TextEditingController(text: modelKarakter.beratKarakter);
    lahirController = TextEditingController(text: modelKarakter.tanggalLahir);

    bool kelamin = (modelKarakter.kelamin == 'male') ? true : false;
    jenisKelamin = modelKarakter.kelamin;

    if (kelamin) {
      _radioValue1 = 0;
    } else {
      _radioValue1 = 1;
    }

    return modelKarakter;
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          jenisKelamin = 'male';
          break;
        case 1:
          jenisKelamin = 'female';
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pop();
        return;
      },
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: ListView(
            children: [
              Container(
                height: 56,
                margin: EdgeInsets.only(top: 20, bottom: 22),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.black,
                          )),
                    ),
                    Center(
                        child: Text(
                            widget.idKarakter != null
                                ? "DETAIL KARAKTER"
                                : "TAMBAH KARAKTER",
                            style: TextStyle(fontSize: 18, color: Colors.black),
                            textAlign: TextAlign.center)),
                  ],
                ),
              ),
              FutureBuilder(
                future: getDataUser(),
                builder: (_, snapshot) {
                  if (snapshot.hasData) {
                    KarakterModel karakterModel = snapshot.data;
                    return ListView(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: [
                        Column(
                          children: [
                            AbsorbPointer(
                              child: TextField(
                                controller: TextEditingController(
                                    text: karakterModel.id.toString()),
                                style: TextStyle(color: Colors.grey),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Karakter ID",
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: nameController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Nama Karakter",
                                  hintText: "Nama Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: tinggiController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Tinggi Karakter",
                                  hintText: "Tinggi Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: beratController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Berat Karakter",
                                  hintText: "Berat Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: lahirController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Berat Karakter",
                                  hintText: "Berat Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                      value: 0,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                    Text(
                                      'Male',
                                      style: new TextStyle(fontSize: 16.0),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Row(
                                  children: [
                                    Radio(
                                      value: 1,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                    Text(
                                      'Female',
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              width: double.infinity,
                              height: 45,
                              child: ElevatedButton(
                                  onPressed: () async {
                                    isDataLengkap =
                                        (nameController.text != null &&
                                                tinggiController.text != null &&
                                                beratController.text != null &&
                                                lahirController.text != null &&
                                                _radioValue1 != -1)
                                            ? true
                                            : false;

                                    if (isDataLengkap) {
                                      final karakterM = karakterModel.copy(
                                        namaKarakter:
                                            nameController.text.toString(),
                                        heightKarakter:
                                            tinggiController.text.toString(),
                                        beratKarakter:
                                            beratController.text.toString(),
                                        tanggalLahir:
                                            nameController.text.toString(),
                                        kelamin: jenisKelamin,
                                      );

                                      showAlertDialog(
                                          context, "Edit Data", 1, karakterM);
                                    } else {
                                      Flushbar(
                                        duration: Duration(milliseconds: 2000),
                                        flushbarPosition:
                                            FlushbarPosition.BOTTOM,
                                        backgroundColor: Colors.orange,
                                        message: "Isi data lengkap !",
                                      )..show(context);
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.orange,
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  child: Text("Update Karakter",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18))),
                            )
                          ],
                        )
                      ],
                    );
                  } else {
                    return ListView(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: nameController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Nama Karakter",
                                  hintText: "Nama Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: tinggiController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Tinggi Karakter",
                                  hintText: "Tinggi Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: beratController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Berat Karakter",
                                  hintText: "Berat Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              controller: lahirController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  labelText: "Tahun Lahir Karakter",
                                  hintText: "Tahun Lahir Karakter"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                      value: 0,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                    Text(
                                      'Male',
                                      style: new TextStyle(fontSize: 16.0),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Row(
                                  children: [
                                    Radio(
                                      value: 1,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                    Text(
                                      'Female',
                                      style: new TextStyle(
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              width: double.infinity,
                              height: 45,
                              child: ElevatedButton(
                                  onPressed: () async {
                                    isDataLengkap =
                                        (nameController.text != null &&
                                                tinggiController.text != null &&
                                                beratController.text != null &&
                                                lahirController.text != null &&
                                                _radioValue1 != -1)
                                            ? true
                                            : false;

                                    if (isDataLengkap) {
                                      final karakterModel = KarakterModel(
                                          namaKarakter:
                                              nameController.text.toString(),
                                          heightKarakter:
                                              tinggiController.text.toString(),
                                          beratKarakter:
                                              beratController.text.toString(),
                                          tanggalLahir:
                                              nameController.text.toString(),
                                          kelamin: jenisKelamin,
                                          films: "",
                                          species: "",
                                          isFavorit: false);

                                      showAlertDialog(context, "Tambah Data", 2,
                                          karakterModel);
                                    } else {
                                      Flushbar(
                                        duration: Duration(milliseconds: 2000),
                                        flushbarPosition:
                                            FlushbarPosition.BOTTOM,
                                        backgroundColor: Colors.orange,
                                        message: "Isi data lengkap !",
                                      )..show(context);
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.orange,
                                      elevation: 0,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  child: Text("Tambah Karakter",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18))),
                            )
                          ],
                        )
                      ],
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  //NOTE : TIPE DIALOG
  // 1 : UPDATE
  // 2: ADD

  void showAlertDialog(BuildContext buildContext, String title, int tipeDialog,
          KarakterModel karakter) =>
      showDialog(
        context: buildContext,
        builder: (_) => AlertDialog(
          title: Text(title),
          content: Text("Are you sure about that ?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(buildContext).pop();
                },
                child: Text("NO")),
            TextButton(
                onPressed: () async {
                  if (tipeDialog == 1) {
                    await KarakterDatabase.instanceDB.update(karakter);
                    print("updateData");
                  } else if (tipeDialog == 2) {
                    await KarakterDatabase.instanceDB.createData(karakter);
                    print("CreateData");
                  }

                  setState(() {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
                  });
                },
                child: Text("YES")),
          ],
          elevation: 24,
        ),
      );
}
