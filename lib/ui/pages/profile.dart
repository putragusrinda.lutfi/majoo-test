import 'package:flutter/material.dart';
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/models/userModel.dart';
import 'package:test_majoo/services/sharedpreference.dart';
import 'package:test_majoo/ui/pages/splashscreen.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: UserPreference.getDataUser(),
        builder: (buildContext, snapshot) {
          if (snapshot.hasData) {
            UserModel userModel = snapshot.data;
            return Scaffold(
              body: Center(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: (userModel.photoUrl == ""
                                  ? Icon(Icons.person)
                                  : NetworkImage(userModel.photoUrl)),
                            )),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        userModel.nameUser,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        userModel.emailUser,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        userModel.phoneNumber ?? "-",
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 70,
                      ),
                      Container(
                        width: 250,
                        height: 45,
                        margin: EdgeInsets.only(top: 120, bottom: 20),
                        child: ElevatedButton(
                          onPressed: () async {
                            showAlertDialog(buildContext);
                          },
                          child: Text("Sign Out !"),
                          style: ElevatedButton.styleFrom(
                              primary: Colors.orange,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25))),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  void showAlertDialog(BuildContext buildContext) => showDialog(
        context: buildContext,
        builder: (_) => AlertDialog(
          title: Text("Sign Out"),
          content: Text("Are you sure about that ?"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(buildContext).pop();
                },
                child: Text("NO")),
            TextButton(
                onPressed: () async {
                  await UserPreference.clearShared();
                  await KarakterDatabase.instanceDB.deleteDB();

                  setState(() {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SplashscreenPage()),
                        (route) => false);
                  });
                },
                child: Text("YES")),
          ],
          elevation: 24,
        ),
      );
}
