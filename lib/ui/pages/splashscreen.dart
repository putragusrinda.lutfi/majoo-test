import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:test_majoo/services/authentication.dart';
import 'package:test_majoo/services/sharedpreference.dart';
import 'package:test_majoo/ui/pages/home.dart';

class SplashscreenPage extends StatefulWidget {
  @override
  _SplashscreenPageState createState() => _SplashscreenPageState();
}

class _SplashscreenPageState extends State<SplashscreenPage> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return !isLoading
        ? Scaffold(
            backgroundColor: Colors.white,
            body: Container(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Center(
                child: Container(
                  width: 250,
                  height: 45,
                  margin: EdgeInsets.only(top: 120, bottom: 20),
                  child: ElevatedButton(
                    onPressed: () async {
                      User user = await Authentication.signInWithGoogle(
                          context: context);

                      if (user != null) {
                        UserPreference.setUserLogin(user, true);

                        setState(() {
                          isLoading = true;
                        });

                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (_) => HomePage()));

                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                    child: Text("Sign In with Google"),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25))),
                  ),
                ),
              ),
            ))
        : SpinKitFadingCircle(
            size: 50,
            color: Colors.orange,
          );
  }
}
