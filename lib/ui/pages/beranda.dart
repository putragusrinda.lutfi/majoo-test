import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/models/karakterModel.dart';
import 'package:test_majoo/ui/pages/detail_karakter.dart';
import 'package:test_majoo/ui/widgets/cardKarakter.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

int countAxis = 1;
bool isSwitched = false;
bool isLoading = false;
bool isSearch = false;
final searchController = TextEditingController();

class _BerandaPageState extends State<BerandaPage> {
  @override
  void initState() {
    super.initState();

    readDatabase();
  }

  Future<List<KarakterModel>> readDatabase() async {
    setState(() => isLoading = true);

    List<KarakterModel> daftar = [];

    if (isSearch) {
      daftar = await KarakterDatabase.instanceDB
          .searchListKarakter(searchController.text.toString());
    } else {
      daftar = await KarakterDatabase.instanceDB.readAllKarakter();
    }

    setState(() => isLoading = false);

    return daftar;
  }

  void filterDatabase() {
    String search = searchController.text.toString() ?? "";

    if (search.length > 0) {
      setState(() {
        isSearch = true;
      });
    } else {
      setState(() {
        isSearch = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: 80),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailKarakterPage()),
            );
          },
          backgroundColor: Colors.orange,
          child: Icon(Icons.add),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: ListView(children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                // color: Colors.amber,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 24),
                          width: MediaQuery.of(context).size.width / 2,
                          child: TextField(
                            controller: searchController,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              filterDatabase();
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 4),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(6)),
                            padding: EdgeInsets.all(8),
                            child: Icon(Icons.search),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Switch(
                            value: isSwitched,
                            activeColor: Colors.orange,
                            onChanged: (value) {
                              setState(() {
                                isSwitched = value;
                                if (value) {
                                  countAxis = 2;
                                } else {
                                  countAxis = 1;
                                }
                              });
                            }),
                        Container(
                            margin: EdgeInsets.only(right: 24),
                            child: Icon(Icons.list))
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: FutureBuilder(
                    future: readDatabase(),
                    builder: (_, snapshot) {
                      if (snapshot.hasData) {
                        List<KarakterModel> daftarKarakter = snapshot.data;
                        return GridView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: countAxis,
                                    childAspectRatio: 3 / 2,
                                    crossAxisSpacing: 20,
                                    mainAxisSpacing: 20),
                            itemCount: daftarKarakter.length,
                            itemBuilder: (BuildContext ctx, index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailKarakterPage(
                                              idKarakter:
                                                  daftarKarakter[index].id,
                                            )),
                                  );
                                },
                                child: CardKarakter(
                                  daftarKarakter[index],
                                  MediaQuery.of(context).size.width / 2,
                                  MediaQuery.of(context).size.height / 5,
                                ),
                              );
                            });
                      } else {
                        return SpinKitFadingCircle(
                          size: 50,
                          color: Colors.orange,
                        );
                      }
                    }),
              ),
              SizedBox(
                height: 80,
              )
            ]),
          ),
        ],
      ),
    );
  }
}
