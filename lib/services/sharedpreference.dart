import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/models/userModel.dart';

class UserPreference {
  static SharedPreferences _sharedPreferences;

  static Future init() async =>
      _sharedPreferences = await SharedPreferences.getInstance();

  static Future setUserLogin(User user, bool isLogin) async {
    final dataUser = await SharedPreferences.getInstance();

    final mappingDataUser = jsonEncode({
      'namaUser': user.displayName,
      'emailUser': user.email,
      'photoUser': user.photoURL,
      'phoneUser': user.phoneNumber,
    });

    dataUser.setBool('isLogin', isLogin);
    dataUser.setString('dataUser', mappingDataUser);
  }

  static Future<bool> getUserLogin() async {
    bool isLogin = false;

    final dataShared = await SharedPreferences.getInstance();

    isLogin = dataShared.getBool('isLogin');

    return isLogin;
  }

  static Future<UserModel> getDataUser() async {
    final dataUser = await jsonDecode(_sharedPreferences.getString('dataUser'))
        as Map<String, dynamic>;

    print(dataUser.toString());

    UserModel userModel = new UserModel(dataUser['namaUser'],
        dataUser['emailUser'], dataUser['phoneUser'], dataUser['photoUser']);

    return userModel;
  }

  static Future clearShared() async {
    _sharedPreferences.clear();
  }
}
