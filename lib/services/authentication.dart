import 'package:another_flushbar/flushbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Authentication {
  static Future<User> signInWithGoogle({BuildContext context}) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    User user;

    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      try {
        final UserCredential userCredential =
            await auth.signInWithCredential(credential);

        user = userCredential.user;
      } on FirebaseAuthException catch (e) {
        Flushbar(
          duration: Duration(seconds: 2),
          flushbarPosition: FlushbarPosition.BOTTOM,
          backgroundColor: Colors.orange,
          message: e.toString(),
        )..show(context);
      } catch (e) {
        Flushbar(
          duration: Duration(seconds: 2),
          flushbarPosition: FlushbarPosition.BOTTOM,
          backgroundColor: Colors.orange,
          message: e.toString(),
        )..show(context);
      }
    }

    return user;
  }
}
