import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_majoo/db/karakter_db.dart';
import 'package:test_majoo/models/karakterModel.dart';

class FetchingData {
  static void getData() async {
    var url = Uri.parse('https://swapi.dev/api/people/');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body) as Map<String, dynamic>;

      List<dynamic> daftarResult = await jsonResponse['results'];

      List<KarakterModel> daftarKarakter = [];

      for (var i = 0; i < daftarResult.length; i++) {
        daftarKarakter.add(new KarakterModel(
            id: i,
            namaKarakter: daftarResult[i]['name'] ?? "",
            heightKarakter: daftarResult[i]['height'] ?? "",
            beratKarakter: daftarResult[i]['mass'] ?? "",
            tanggalLahir: daftarResult[i]['birth_year'] ?? "",
            kelamin: daftarResult[i]['gender'] ?? "",
            films: daftarResult[i]['films'].toString() ?? "",
            species: daftarResult[i]['species'].toString() ?? "",
            isFavorit: false));
      }

      for (var i = 0; i < daftarKarakter.length; i++) {
        KarakterDatabase.instanceDB.createData(daftarKarakter[i]);
      }

      int sizeTabelAkhir = await KarakterDatabase.instanceDB.countSizeTable();

      print("Size Tabel $sizeTabelAkhir");
    } else {
      print("error get Data");
    }
  }
}
