final String namaTabel = 'tabelKarakter';

class KarakterFields {
  static final List<String> values = [
    id,
    namaKarakter,
    heightKarakter,
    beratKarakter,
    tanggalLahir,
    kelamin,
    films,
    species,
    isFavorit
  ];

  static final String id = '_id';
  static final String namaKarakter = 'namaKarakter';
  static final String heightKarakter = 'heightKarakter';
  static final String beratKarakter = 'beratKarakter';
  static final String tanggalLahir = 'tanggalLahir';
  static final String kelamin = 'kelamin';
  static final String films = 'films';
  static final String species = 'species';
  static final String isFavorit = 'isFavorit';
}

class KarakterModel {
  final int id;
  final String namaKarakter;
  final String heightKarakter;
  final String beratKarakter;
  final String tanggalLahir;
  final String kelamin;
  final String films;
  final String species;
  final bool isFavorit;

  const KarakterModel(
      {this.id,
      this.namaKarakter,
      this.heightKarakter,
      this.beratKarakter,
      this.tanggalLahir,
      this.kelamin,
      this.films,
      this.species,
      this.isFavorit});

  KarakterModel copy(
          {int id,
          String namaKarakter,
          String heightKarakter,
          String beratKarakter,
          String tanggalLahir,
          String kelamin,
          String films,
          String species,
          bool isFavorit}) =>
      KarakterModel(
          id: id ?? this.id,
          namaKarakter: namaKarakter ?? this.namaKarakter,
          heightKarakter: heightKarakter ?? this.heightKarakter,
          beratKarakter: beratKarakter ?? this.beratKarakter,
          tanggalLahir: tanggalLahir ?? this.tanggalLahir,
          kelamin: kelamin ?? this.kelamin,
          films: films ?? this.films,
          species: species ?? this.species,
          isFavorit: isFavorit ?? this.isFavorit);

  static KarakterModel fromJson(Map<String, Object> json) => KarakterModel(
      id: json[KarakterFields.id] as int,
      namaKarakter: json[KarakterFields.namaKarakter] as String,
      heightKarakter: json[KarakterFields.heightKarakter] as String,
      beratKarakter: json[KarakterFields.beratKarakter] as String,
      tanggalLahir: json[KarakterFields.tanggalLahir] as String,
      kelamin: json[KarakterFields.kelamin] as String,
      films: json[KarakterFields.films] as String,
      species: json[KarakterFields.species] as String,
      isFavorit: json[KarakterFields.isFavorit] == 1);

  Map<String, Object> toJson() => {
        KarakterFields.id: id,
        KarakterFields.namaKarakter: namaKarakter,
        KarakterFields.heightKarakter: heightKarakter,
        KarakterFields.beratKarakter: beratKarakter,
        KarakterFields.tanggalLahir: tanggalLahir,
        KarakterFields.kelamin: kelamin,
        KarakterFields.films: films,
        KarakterFields.species: species,
        KarakterFields.isFavorit: isFavorit ? 1 : 0
      };
}
