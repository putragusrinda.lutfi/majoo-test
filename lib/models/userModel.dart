class UserModel {
  final String nameUser;
  final String emailUser;
  final String phoneNumber;
  final String photoUrl;

  UserModel(this.nameUser, this.emailUser, this.phoneNumber, this.photoUrl);

  List<Object> get props => [
        nameUser,
        emailUser,
        phoneNumber,
        photoUrl,
      ];
}
